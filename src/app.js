(
    function () {
        // Run function
        var _app_run = function () {

        };

        // Configuration function
        var _app_config = function ($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state('main', {
                    url: '/main',
                    templateUrl: 'src/views/main/main.template.html',
                    controller: 'main-controller'
                });

            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/main');
        };

        // Injects the Dependencies (in a way that can be compressed)
        _app_run.$inject = [];
        _app_config.$inject = ['$stateProvider', '$urlRouterProvider'];

        // creates the base modules
        angular.module('TestElectronApp.Views', []);

        // Declares the modules
        var _app_dependencies = [
            'ui.router',
            'TestElectronApp.Views'
        ];

        // declares the app and runs the configuration and run methods
        angular.module('TestElectronApp', _app_dependencies).run(_app_run).config(_app_config);
    }
)();
