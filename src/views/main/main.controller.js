(
    function () {
        var _main_controller = function ($scope) {
            $scope.name = 'Hello World!!';
        };

        _main_controller.$inject = [ '$scope' ];

        angular.module('TestElectronApp.Views').controller('main-controller', _main_controller);
    }
)();
